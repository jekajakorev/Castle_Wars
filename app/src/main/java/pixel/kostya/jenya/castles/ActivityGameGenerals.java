package pixel.kostya.jenya.castles;


import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ActivityGameGenerals extends Activity {
    ImageView arrayOfImage[];
    int arrayOfGame[]={0};
    int Colomns,Rows,Turns;
    int player = 1;
    boolean redwinner=false;
    boolean firsttop = true;
    ImageView playerrun;
    TextView text;
    boolean topforcity = false;
    int generalx, generaly, spawngreen,spawnred;
    boolean generalmove = false;
    boolean canendpart;
    int generalgreen[];
    int generalred[];
    //Map<Integer,Integer> generalsgreen = new HashMap<Integer,Integer>();
    //Map<Integer,Integer> generalsred = new HashMap<Integer,Integer>();
    public void generalreturn(){
        for (int i = 0;i<Rows*Colomns;i++)
            {
                if (generalgreen[i]>0)
                    generalgreen[i]=3;
                if (generalred[i]>0)
                    generalred[i]=3;
            }
    }
    public boolean youcontrolarround(int y,int x){
        if (player==1)
        {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i) * Colomns + j - 1 + x] == 1) ||
                                    ((arrayOfGame[(y + i) * Colomns + j - 1 + x] > 13) && (arrayOfGame[(y + i) * Colomns + j - 1 + x] < 20))||(arrayOfGame[(y + i) * Colomns + j - 1 + x] == 23)||(arrayOfGame[(y + i) * Colomns + j - 1 + x] == 12)||(arrayOfGame[(y + i) * Colomns + j - 1 + x] == 20)) {
                                return true;
                            }
                    }
                }
                if (y - 1 >= 0)
                    if ((arrayOfGame[(y - 1) * Colomns + x] == 1) ||
                            ((arrayOfGame[(y - 1) * Colomns + x] > 13) && (arrayOfGame[(y - 1) * Colomns + x] < 20))||(arrayOfGame[(y - 1) * Colomns + x] == 23)||(arrayOfGame[(y - 1) * Colomns + x] == 12)||(arrayOfGame[(y - 1) * Colomns + x] == 20))
                        return true;
            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 1) ||
                                    ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] > 13) && (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] < 20))||(arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 23)||(arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 12)||(arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 20)) {
                                return true;
                            }
                    }
                }
                if (y + 1 < Rows)
                    if ((arrayOfGame[(y + 1) * Colomns + x] == 1) ||
                            ((arrayOfGame[(y + 1) * Colomns + x] > 13) && (arrayOfGame[(y + 1) * Colomns + x] < 20))||(arrayOfGame[(y + 1) * Colomns + x] == 23)||(arrayOfGame[(y + 1) * Colomns + x] == 12)||(arrayOfGame[(y + 1) * Colomns + x] == 20))
                        return true;
            }
        }
        if (player == 2) {
            if (!(x % 2 == 0)) {
                if (y - 1 >= 0)
                    if ((arrayOfGame[(y - 1) * Colomns + x] == 2) ||
                            ((arrayOfGame[(y - 1) * Colomns + x] > 23) && (arrayOfGame[(y - 1) * Colomns + x] < 30))||(arrayOfGame[(y - 1) * Colomns + x] == 13)||(arrayOfGame[(y - 1) * Colomns + x] == 10)||(arrayOfGame[(y - 1) * Colomns + x] == 22))
                        return true;
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i) * Colomns + j - 1 + x] == 2) ||
                                    ((arrayOfGame[(y + i) * Colomns + j - 1 + x] > 23) && (arrayOfGame[(y + i) * Colomns + j - 1 + x] < 30))||(arrayOfGame[(y + i) * Colomns + j - 1 + x] == 13)||(arrayOfGame[(y + i) * Colomns + j - 1 + x] == 10)||(arrayOfGame[(y + i) * Colomns + j - 1 + x] == 22)) {
                                return true;
                            }
                    }
                }
            }


            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 2) ||
                                    ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] > 23) && (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] < 30))||(arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 13)||(arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 10)||(arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 22)) {
                                return true;
                            }
                    }
                }
                if (y + 1 < Rows)
                    if ((arrayOfGame[(y + 1) * Colomns + x] == 2) ||
                            ((arrayOfGame[(y + 1) * Colomns + x] > 23) && (arrayOfGame[(y + 1) * Colomns + x] < 30))||(arrayOfGame[(y + 1) * Colomns + x] == 13)||(arrayOfGame[(y + 1) * Colomns + x] == 10)||(arrayOfGame[(y + 1) * Colomns + x] == 22))
                        return true;
            }
        }
        return false;
    }
    public void addmycontrol(int y,int x) {
        if (player==1) {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)) {
                            switch (arrayOfGame[(y + i) * Colomns + j - 1 + x]) {
                                case 4: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 5: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 28:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 16; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_green); break;
                                case 26:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 18; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_green); break;
                                case 29:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 9: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 27:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 0:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 11:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 15; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1green); break;
                                case 13:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 15; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1green); break;
                                case 21:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 23; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_green); break;
                                case 25:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 23; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_green); break;
                                case 31:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 32; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1green); break;
                                case 33:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 32; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1green); break;
                                case 41:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 42; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2green); break;
                                case 43:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 42; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2green); break;
                                case 10:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 12; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl1_pl1); break;
                                case 22:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 20; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl2_pl1); break;



                            }
                        }
                    }
                }
                if (y - 1 >= 0){
                    switch (arrayOfGame[(y - 1) * Colomns + x]) {
                        case 4: arrayOfGame[(y - 1) * Colomns + x] = 3; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 5: arrayOfGame[(y - 1) * Colomns + x] = 3; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 28:arrayOfGame[(y - 1) * Colomns + x] = 16; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_green); break;
                        case 26:arrayOfGame[(y - 1) * Colomns + x] = 18; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_green); break;
                        case 29:arrayOfGame[(y - 1) * Colomns + x] = 19; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 9: arrayOfGame[(y - 1) * Colomns + x] = 19; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 27:arrayOfGame[(y - 1) * Colomns + x] = 17; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 0:arrayOfGame[(y - 1) * Colomns + x] = 17; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 11:arrayOfGame[(y - 1) * Colomns + x] = 15; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl1green); break;
                        case 13:arrayOfGame[(y - 1) * Colomns + x] = 15; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl1green); break;
                        case 21:arrayOfGame[(y - 1) * Colomns + x] = 23; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl2_green); break;
                        case 25:arrayOfGame[(y - 1) * Colomns + x] = 23; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl2_green); break;
                        case 31:arrayOfGame[(y - 1) * Colomns + x] = 32; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1green); break;
                        case 33:arrayOfGame[(y - 1) * Colomns + x] = 32; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1green); break;
                        case 41:arrayOfGame[(y - 1) * Colomns + x] = 42; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2green); break;
                        case 43:arrayOfGame[(y - 1) * Colomns + x] = 42; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2green); break;
                        case 10:arrayOfGame[(y - 1) * Colomns + x] = 12; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_castlepl1_pl1); break;
                        case 22:arrayOfGame[(y - 1) * Colomns + x] = 20; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_castlepl2_pl1); break;
                    }
                }

            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)){
                            switch (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x])  {
                                case 4: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 5: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 28:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 16; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_green); break;
                                case 26:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 18; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_green); break;
                                case 29:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 9: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 27:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 0:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 11:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 15; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1green); break;
                                case 13:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 15; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1green); break;
                                case 21:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 23; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_green); break;
                                case 25:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 23; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_green); break;
                                case 31:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 32; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1green); break;
                                case 33:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 32; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1green); break;
                                case 41:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 42; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2green); break;
                                case 43:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 42; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2green); break;
                                case 10:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 12; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl1_pl1); break;
                                case 22:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 20; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl2_pl1); break;
                            }
                        }
                    }
                }
                if (y + 1 < Rows)
                {
                    switch (arrayOfGame[(y + 1) * Colomns + x]){
                        case 4: arrayOfGame[(y + 1) * Colomns + x] = 3; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 5: arrayOfGame[(y + 1) * Colomns + x] = 3; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 28:arrayOfGame[(y + 1) * Colomns + x] = 16; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_green); break;
                        case 26:arrayOfGame[(y + 1) * Colomns + x] = 18; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_green); break;
                        case 29:arrayOfGame[(y + 1) * Colomns + x] = 19; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 9: arrayOfGame[(y + 1) * Colomns + x] = 19; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 27:arrayOfGame[(y + 1) * Colomns + x] = 17; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 0:arrayOfGame[(y + 1) * Colomns + x] = 17; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 11:arrayOfGame[(y + 1) * Colomns + x] = 15; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl1green); break;
                        case 13:arrayOfGame[(y + 1) * Colomns + x] = 15; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl1green); break;
                        case 21:arrayOfGame[(y + 1) * Colomns + x] = 23; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl2_green); break;
                        case 25:arrayOfGame[(y + 1) * Colomns + x] = 23; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl2_green); break;
                        case 31:arrayOfGame[(y + 1) * Colomns + x] = 32; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1green); break;
                        case 33:arrayOfGame[(y + 1) * Colomns + x] = 32; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1green); break;
                        case 41:arrayOfGame[(y + 1) * Colomns + x] = 42; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2green); break;
                        case 43:arrayOfGame[(y + 1) * Colomns + x] = 42; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2green); break;
                        case 10:arrayOfGame[(y + 1) * Colomns + x] = 12; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_castlepl1_pl1); break;
                        case 22:arrayOfGame[(y + 1) * Colomns + x] = 20; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_castlepl2_pl1); break;

                    }
                }

            }

        }
        if (player==2) {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)) {
                            switch (arrayOfGame[(y + i) * Colomns + j - 1 + x]) {
                                case 3: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 5: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 18:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 26; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_red); break;
                                case 16:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 28; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_red); break;
                                case 19:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 9: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 17:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 0:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 21:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 25; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_red); break;
                                case 23:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 25; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_red); break;
                                case 11:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 13; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1red); break;
                                case 15:arrayOfGame[(y + i) * Colomns + j - 1 + x]  = 13; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1red); break;
                                case 31:arrayOfGame[(y + i) * Colomns + j - 1 + x]  = 33; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1red); break;
                                case 32:arrayOfGame[(y + i) * Colomns + j - 1 + x]  = 33; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1red); break;
                                case 41:arrayOfGame[(y + i) * Colomns + j - 1 + x]  = 43; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2red); break;
                                case 42:arrayOfGame[(y + i) * Colomns + j - 1 + x]  = 43; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2red); break;
                                case 12:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 10; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl2_pl1); break;
                                case 20:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 22; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl2_pl2); break;

                            }
                        }
                    }
                }
                if (y - 1 >= 0){
                    switch (arrayOfGame[(y - 1) * Colomns + x]) {
                        case 3: arrayOfGame[(y - 1) * Colomns + x] = 4; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 5: arrayOfGame[(y - 1) * Colomns + x] = 4; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 18:arrayOfGame[(y - 1) * Colomns + x] = 26; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_red); break;
                        case 16:arrayOfGame[(y - 1) * Colomns + x] = 28; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_red); break;
                        case 19:arrayOfGame[(y - 1) * Colomns + x] = 29; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 9: arrayOfGame[(y - 1) * Colomns + x] = 29; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 17:arrayOfGame[(y - 1) * Colomns + x] = 27; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 0:arrayOfGame[(y - 1) * Colomns + x] = 27; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 21:arrayOfGame[(y - 1) * Colomns + x] = 25; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl2_red); break;
                        case 23:arrayOfGame[(y - 1) * Colomns + x] = 25; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl2_red); break;
                        case 11:arrayOfGame[(y - 1) * Colomns + x] = 13; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl1red); break;
                        case 15:arrayOfGame[(y - 1) * Colomns + x]  = 13; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_pl1red); break;
                        case 31:arrayOfGame[(y - 1) * Colomns + x]  = 33; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1red); break;
                        case 32:arrayOfGame[(y - 1) * Colomns + x]  = 33; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1red); break;
                        case 41:arrayOfGame[(y - 1) * Colomns + x]  = 43; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2red); break;
                        case 42:arrayOfGame[(y - 1) * Colomns + x]  = 43; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2red); break;
                        case 12:arrayOfGame[(y - 1) * Colomns + x] = 10; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_castlepl2_pl1); break;
                        case 20:arrayOfGame[(y - 1) * Colomns + x] = 22; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.general_castlepl2_pl2); break;

                    }
                }

            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)){
                            switch (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x])  {
                                case 3: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 5: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 18:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 26; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_red); break;
                                case 16:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 28; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_red); break;
                                case 19:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 9: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 17:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 0:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 21:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 25; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_red); break;
                                case 23:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 25; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl2_red); break;
                                case 11:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 13; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1red); break;
                                case 15:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x]  = 13; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_pl1red); break;
                                case 31:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x]  = 33; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1red); break;
                                case 32:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x]  = 33; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl1red); break;
                                case 41:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x]  = 43; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2red); break;
                                case 42:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x]  = 43; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.generalwater_pl2red); break;
                                case 12:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 10; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl2_pl1); break;
                                case 20:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 22; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.general_castlepl2_pl2); break;
                            }
                        }
                    }
                }
                if (y + 1 < Rows)
                {
                    switch (arrayOfGame[(y + 1) * Colomns + x]){
                        case 3: arrayOfGame[(y + 1) * Colomns + x] = 4; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 5: arrayOfGame[(y + 1) * Colomns + x] = 4; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 18:arrayOfGame[(y + 1) * Colomns + x] = 26; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_red); break;
                        case 16:arrayOfGame[(y + 1) * Colomns + x] = 28; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_red); break;
                        case 19:arrayOfGame[(y + 1) * Colomns + x] = 29; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 9: arrayOfGame[(y + 1) * Colomns + x] = 29; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 17:arrayOfGame[(y + 1) * Colomns + x] = 27; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 0:arrayOfGame[(y + 1) * Colomns + x] = 27; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 21:arrayOfGame[(y + 1) * Colomns + x] = 25; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl2_red); break;
                        case 23:arrayOfGame[(y + 1) * Colomns + x] = 25; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl2_red); break;
                        case 11:arrayOfGame[(y + 1) * Colomns + x] = 13; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl1red); break;
                        case 15:arrayOfGame[(y + 1) * Colomns + x] = 13; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_pl1red); break;
                        case 31:arrayOfGame[(y + 1) * Colomns + x]  = 33; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1red); break;
                        case 32:arrayOfGame[(y + 1) * Colomns + x]  = 33; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl1red); break;
                        case 41:arrayOfGame[(y + 1) * Colomns + x]  = 43; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2red); break;
                        case 42:arrayOfGame[(y + 1) * Colomns + x]  = 43; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.generalwater_pl2red); break;
                        case 12:arrayOfGame[(y + 1) * Colomns + x] = 10; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_castlepl2_pl1); break;
                        case 20:arrayOfGame[(y + 1) * Colomns + x] = 22; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.general_castlepl2_pl2); break;
                    }
                }
            }
        }
    }
    public void vozrat(){
        switch (arrayOfGame[generaly*Colomns+generalx]){
            case 14: arrayOfGame[generaly*Colomns+generalx]=1;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.city_pl1);break;
            case 11: arrayOfGame[generaly*Colomns+generalx]=0;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.card);break;
            case 15: arrayOfGame[generaly*Colomns+generalx]=17;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.card_pl1);break;
            case 12: arrayOfGame[generaly*Colomns+generalx]=16;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.castle_green);break;
            case 10: arrayOfGame[generaly*Colomns+generalx]=26;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.castle_red);break;
            case 13: arrayOfGame[generaly*Colomns+generalx]=27;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.card_pl2);break;
            case 24: arrayOfGame[generaly*Colomns+generalx]=2;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.city_pl2);break;
            case 25: arrayOfGame[generaly*Colomns+generalx]=27;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.card_pl2);break;
            case 23: arrayOfGame[generaly*Colomns+generalx]=17;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.card_pl1);break;
            case 21: arrayOfGame[generaly*Colomns+generalx]=0;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.card);break;
            case 20: arrayOfGame[generaly*Colomns+generalx]=16;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.castle_green);break;
            case 22: arrayOfGame[generaly*Colomns+generalx]=26;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.castle_red);break;
            case 31: arrayOfGame[generaly * Colomns + generalx]=9;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.river_nopl);break;
            case 33: arrayOfGame[generaly * Colomns + generalx]=29;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.river_pl2);break;
            case 32: arrayOfGame[generaly * Colomns + generalx]=19;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.river_pl1);break;
            case 41: arrayOfGame[generaly * Colomns + generalx]=9;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.river_nopl);break;
            case 42: arrayOfGame[generaly * Colomns + generalx]=19;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.river_pl1);break;
            case 43: arrayOfGame[generaly * Colomns + generalx]=29;arrayOfImage[generaly*Colomns+generalx].setImageResource(R.drawable.river_pl2 );break;
        }

    }
    public void addgeneral(int generaltoy,int generaltox){
        if (player==1) {
            switch (arrayOfGame[generaltoy * Colomns + generaltox]) {
                case 0: arrayOfGame[generaltoy * Colomns + generaltox]=11;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_pl1);break;
                case 16: arrayOfGame[generaltoy * Colomns + generaltox]=12;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl1_pl1);break;
                case 18: arrayOfGame[generaltoy * Colomns + generaltox]=12;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl1_pl1);break;
                case 17: arrayOfGame[generaltoy * Colomns + generaltox]=15;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_pl1green);break;
                case 13: arrayOfGame[generaltoy * Colomns + generaltox]=15;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_pl1green);break;
                case 26: arrayOfGame[generaltoy * Colomns + generaltox]=10;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl2_pl1);break;
                case 28: arrayOfGame[generaltoy * Colomns + generaltox]=10;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl2_pl1);break;
                case 27: arrayOfGame[generaltoy * Colomns + generaltox]=13;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_pl1red);break;
                case 9: arrayOfGame[generaltoy * Colomns + generaltox]=31;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.generalwater_pl1);break;
                case 19: arrayOfGame[generaltoy * Colomns + generaltox]=32;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.generalwater_pl1green);break;
                case 29: arrayOfGame[generaltoy * Colomns + generaltox]=33;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.generalwater_pl1red);break;



            }
        }
        if (player==2) {
            switch (arrayOfGame[generaltoy * Colomns + generaltox]) {
                case 0: arrayOfGame[generaltoy * Colomns + generaltox]=21;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_pl2);break;
                case 26: arrayOfGame[generaltoy * Colomns + generaltox]=22;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl2_pl2);break;
                case 28: arrayOfGame[generaltoy * Colomns + generaltox]=22;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl2_pl2);break;
                case 27: arrayOfGame[generaltoy * Colomns + generaltox]=25;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_pl2_red);break;
                case 16: arrayOfGame[generaltoy * Colomns + generaltox]=20;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl1_pl2);break;
                case 18: arrayOfGame[generaltoy * Colomns + generaltox]=20;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_castlepl1_pl2);break;
                case 17: arrayOfGame[generaltoy * Colomns + generaltox]=23;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.general_pl2_green);break;
                case 9: arrayOfGame[generaltoy * Colomns + generaltox]=41;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.generalwater_pl2);break;
                case 19: arrayOfGame[generaltoy * Colomns + generaltox]=42;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.generalwater_pl2green);break;
                case 29: arrayOfGame[generaltoy * Colomns + generaltox]=43;arrayOfImage[generaltoy*Colomns+generaltox].setImageResource(R.drawable.generalwater_pl2red);break;
            }
        }
    }
    public boolean canput2(int generaly,int generalx,int generaltoy,int generaltox,boolean type){
        if (!(generalx % 2 == 0)) {
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 3; j++) {
                    if (((generaly + i) * Colomns + j - 1 + generalx>=0)&&((generaly + i) * Colomns + j - 1 + generalx<Colomns*Rows))
                        if (!((arrayOfGame[(generaly + i) * Colomns + j - 1 + generalx]>2)&&(arrayOfGame[(generaly + i) * Colomns + j - 1 + generalx]<6))){
                            if ((generaltoy==generaly + i)&&(generaltox==j - 1+generalx)) {if (type){minus(generaltoy,generaltox,2);} return true;}
                        }
                }
            }
        }
        if ((generaly - 1) * Colomns + generalx >= 0){
            if (!((arrayOfGame[(generaly - 1) * Colomns + generalx]>2)&&(arrayOfGame[(generaly - 1) * Colomns + generalx]<6))){
                if ((generaltoy==generaly - 1)&&(generaltox==generalx)) {if (type){minus(generaltoy,generaltox,2);} return true;}
            }

        }
        if (!(generalx % 2 != 0)) {
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 3; j++) {
                    if (((generaly + i - 1) * Colomns + j - 1 + generalx>=0)&&((generaly + i - 1) * Colomns + j - 1 + generalx<Colomns*Rows)) {
                        if (!((arrayOfGame[(generaly + i - 1) * Colomns + j - 1 + generalx] > 2) && (arrayOfGame[(generaly + i - 1) * Colomns + j - 1 + generalx] < 6))) {
                            if ((generaltoy == generaly + i - 1) && (generaltox == j - 1 + generalx)){
                                if (type){minus(generaltoy,generaltox,2);} return true;}
                        }
                    }
                }
            }
            if (((generaly + 1) * Colomns + generalx>=0)&&((generaly + 1) * Colomns + generalx<Colomns*Rows)) {
                if (!((arrayOfGame[(generaly + 1) * Colomns + generalx] > 2) && (arrayOfGame[(generaly + 1) * Colomns + generalx] < 6))) {
                    if ((generaltoy == generaly + 1) && (generaltox == generalx)) {if (type){minus(generaltoy,generaltox,2);} return true;}
                }
            }
        }
        return false;
    }
    public void minus(int generaltoy,int generaltox,int type){
        if (type==1) {
            if (player == 1) {
                generalgreen[generaltoy * Colomns + generaltox] = generalgreen[generaly * Colomns + generalx] - 1;
                generalgreen[generaly * Colomns + generalx] = 0;
                return;
            }
            if (player == 2) {
                generalred[generaltoy * Colomns + generaltox] = generalred[generaly * Colomns + generalx] - 1;
                generalred[generaly * Colomns + generalx] = 0;
                return;
            }
        }else {
                if (player == 1) {
                    generalgreen[generaltoy * Colomns + generaltox] = 1;
                    generalgreen[generaly * Colomns + generalx] = 0;
                    return;
                }
                if (player == 2) {
                    generalred[generaltoy * Colomns + generaltox] = 1;
                    generalred[generaly * Colomns + generalx] = 0;
                    return;
                }
        }


    }
    public boolean canput(int generaltoy,int generaltox,boolean type){
            if (!(generalx % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if (((generaly + i) * Colomns + j - 1 + generalx>=0)&&((generaly + i) * Colomns + j - 1 + generalx<Colomns*Rows))
                            if (!((arrayOfGame[(generaly + i) * Colomns + j - 1 + generalx]>2)&&(arrayOfGame[(generaly + i) * Colomns + j - 1 + generalx]<6))){
                                if ((generaltoy==generaly + i)&&(generaltox==j - 1+generalx)) {if (type){minus(generaltoy,generaltox,1);}; return true;}
                            }
                        }
                    }
                }
                if (((generaly - 1) * Colomns + generalx >= 0)&&((generaly - 1) * Colomns + generalx <Colomns*Rows)){
                    if (!((arrayOfGame[(generaly - 1) * Colomns + generalx]>2)&&(arrayOfGame[(generaly - 1) * Colomns + generalx]<6))){
                    if ((generaltoy==generaly - 1)&&(generaltox==generalx)) {if (type){minus(generaltoy,generaltox,1);} return true;}
                }

            }
            if (!(generalx % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if (((generaly + i - 1) * Colomns + j - 1 + generalx>=0)&&((generaly + i - 1) * Colomns + j - 1 + generalx<Colomns*Rows)) {
                            if (!((arrayOfGame[(generaly + i - 1) * Colomns + j - 1 + generalx] > 2) && (arrayOfGame[(generaly + i - 1) * Colomns + j - 1 + generalx] < 6))) {
                                if ((generaltoy == generaly + i - 1) && (generaltox == j - 1 + generalx)){
                                    if (type){minus(generaltoy,generaltox,1);}  return true;}
                            }
                        }
                    }
                }
                if (((generaly + 1) * Colomns + generalx>=0)&&((generaly + 1) * Colomns + generalx<Colomns*Rows)) {
                    if (!((arrayOfGame[(generaly + 1) * Colomns + generalx] > 2) && (arrayOfGame[(generaly + 1) * Colomns + generalx] < 6))) {
                        if ((generaltoy == generaly + 1) && (generaltox == generalx)){ if (type){minus(generaltoy,generaltox,1);}  return true;}
                    }
                }
            }
        if (!(generalx % 2 == 0)) {
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 3; j++) {
                    if (((generaly + i) * Colomns + j - 1 + generalx>=0)&&((generaly + i) * Colomns + j - 1 + generalx<Colomns*Rows))
                        if (!((arrayOfGame[(generaly + i) * Colomns + j - 1 + generalx]>2)&&(arrayOfGame[(generaly + i) * Colomns + j - 1 + generalx]<6))){
                            if ((generalgreen[generaly*Colomns+generalx]==3)||(generalred[generaly*Colomns+generalx]==3))
                                if (canput2(generaly + i,j - 1 + generalx,generaltoy,generaltox,type)) return true;}
                        }
            }
        }
        if (((generaly - 1) * Colomns + generalx >= 0)&&((generaly - 1) * Colomns + generalx <Colomns*Rows)){
            if (!((arrayOfGame[(generaly - 1) * Colomns + generalx]>2)&&(arrayOfGame[(generaly - 1) * Colomns + generalx]<6))){
                if ((generalgreen[generaly*Colomns+generalx]==3)||(generalred[generaly*Colomns+generalx]==3))
                    if (canput2(generaly - 1, generalx,generaltoy,generaltox,type)) return true;
            }

        }
        if (!(generalx % 2 != 0)) {
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 3; j++) {
                    if (((generaly + i - 1) * Colomns + j - 1 + generalx>=0)&&((generaly + i - 1) * Colomns + j - 1 + generalx<Colomns*Rows)) {
                        if (!((arrayOfGame[(generaly + i - 1) * Colomns + j - 1 + generalx] > 2) && (arrayOfGame[(generaly + i - 1) * Colomns + j - 1 + generalx] < 6))) {
                            if ((generalgreen[generaly*Colomns+generalx]==3)||(generalred[generaly*Colomns+generalx]==3))
                                if (canput2(generaly + i - 1, j - 1 + generalx, generaltoy, generaltox,type)) return true;
                        }
                    }
                }
            }
            if (((generaly + 1) * Colomns + generalx>=0)&&((generaly + 1) * Colomns + generalx<Colomns*Rows)) {
                if (!((arrayOfGame[(generaly + 1) * Colomns + generalx] > 2) && (arrayOfGame[(generaly + 1) * Colomns + generalx] < 6))) {
                    if ((generalgreen[generaly*Colomns+generalx]==3)||(generalred[generaly*Colomns+generalx]==3))
                        if (canput2(generaly + 1, generalx, generaltoy, generaltox,type))  return true;
                }
            }
        }
        return false;
    }

    public void generalmovement(int generaltoy,int generaltox) {
        if ((generalgreen[generaly * Colomns + generalx] > 1) || (generalred[generaly * Colomns + generalx] > 1)) {
            if ((arrayOfGame[generaltoy * Colomns + generaltox] == 0) || ((arrayOfGame[generaltoy * Colomns + generaltox] > 15) && (arrayOfGame[generaltoy * Colomns + generaltox] < 20)) || ((arrayOfGame[generaltoy * Colomns + generaltox] > 25)&&(arrayOfGame[generaltoy*Colomns+generaltox]<31))||(arrayOfGame[generaltoy * Colomns + generaltox]==9)) {
                if (canput(generaltoy, generaltox,true)) {
                    if ((spawngreen==generaly*Colomns+generalx)||(spawnred==generaly*Colomns+generalx))
                    canendpart = true;
                    if (player == 1) {
                        //if (generalgreen[generaly * Colomns + generalx] > 1) {
                        if (arrayOfGame[generaltoy * Colomns + generaltox] != 14) {
                            vozrat();
                            addgeneral(generaltoy, generaltox);
                            generalmove = false;
                        }
                        //}  view.animate().x(where);
                    } else {
                        //if (generalred[generaly * Colomns + generalx] > 1) {
                        if (arrayOfGame[generaltoy * Colomns + generaltox] != 24) {
                            vozrat();
                            addgeneral(generaltoy, generaltox);
                            generalmove = false;
                        }
                        //}
                    }
                }else Toast.makeText(ActivityGameGenerals.this,"You can't move general there",Toast.LENGTH_SHORT).show();
            }
        }else Toast.makeText(ActivityGameGenerals.this,"You can't move this general",Toast.LENGTH_SHORT).show();
    }
    public void scan(){
        int playero=0;
        int playert=0;
        for (int i=0;i<Rows;i++)
            for (int j=0;j<Colomns;j++){
                if ((arrayOfGame[i*Colomns+j] == 1) ||
                        ((arrayOfGame[i*Colomns+j] > 13) && (arrayOfGame[i*Colomns+j] < 20))||(arrayOfGame[i*Colomns+j] == 23)||(arrayOfGame[i*Colomns+j] == 12)||(arrayOfGame[i*Colomns+j] == 20))
                    playero++;
                if ((arrayOfGame[i*Colomns+j] == 2) ||
                        ((arrayOfGame[i*Colomns+j] > 23) && (arrayOfGame[i*Colomns+j] < 30))||(arrayOfGame[i*Colomns+j] == 13)||(arrayOfGame[i*Colomns+j] == 10)||(arrayOfGame[i*Colomns+j] == 22))
                    playert++;
            }
        TextView textgreen = (TextView)findViewById(R.id.textView7);
        textgreen.setText(Integer.toString(playero));
        TextView textred = (TextView)findViewById(R.id.textView);
        textred.setText(Integer.toString(playert));
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(Integer.toString(Turns));


        if (playero<playert)
            redwinner=true;
        else
            redwinner=false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        playerrun = (ImageView) findViewById(R.id.playerrun);
        text = (TextView) findViewById(R.id.textView);
        Colomns = getIntent().getExtras().getInt("Colomns");
        Rows = getIntent().getExtras().getInt("Rows");
        Turns = getIntent().getExtras().getInt("Turns");
        arrayOfImage = new ImageView[Colomns * Rows];
        arrayOfGame = getIntent().getExtras().getIntArray("arraygame");
        generalgreen = new int[Colomns*Rows];
        generalred=new int[Colomns*Rows];
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(Integer.toString(Turns));

        topforcity = false;



        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int scalex = metrics.widthPixels;
        int scaley = metrics.heightPixels;
        float dp = metrics.density;
        int scale;
        if (scalex < scaley) scale = scaley;
        else scale = scalex;
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView);
        RelativeLayout flay = new RelativeLayout(getApplicationContext());
        int height=(int)(scale*Rows*0.089+0.0435*scale);
        int width=(int)(0.078*Colomns * scale+0.025*scale);
        scrollView.addView(flay,width,height);
//        Log.d("ImageView3:", "Width"+(((ImageView)findViewById(R.id.ImageView3)).getWidth()));
        if (width < scalex-119*dp-106*dp) {
            scrollView.setX((int) (((scalex-width-119*dp-106*dp)/2)));}
        if (height < scaley){
            scrollView.setY((int)((scaley-height)/2));}

        /*if (scale * Colomns * 0.074 <= scalex - scale * 0.08*2) {

            lparams.width = (int) (scale * Colomns * 0.08);
            //lparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        } //else{
            //lparams.addRule(RelativeLayout.RIGHT_OF,R.id.ImageView3);
            //lparams.addRule(RelativeLayout.ALIGN_TOP,R.id.playerrun);
            //lparams.addRule(RelativeLayout.START_OF,R.id.playerrun);
            //lparams.addRule(RelativeLayout.END_OF,R.id.imageView);
        //}
        if (scale*Rows*0.087+0.0435*scale <= scaley){
            lparams.height = (int)(scale*Rows*0.087+0.0435*scale);
            //lparams.addRule(RelativeLayout.CENTER_VERTICAL);
        }
        scrollView.setLayoutParams(lparams);*/

        for (int i=0;i<Rows;i++){
            int b=0;
            for (int j=0;j<Colomns;j++){
                arrayOfImage[i*Colomns+j] = new ImageView(getApplicationContext());
                flay.addView(arrayOfImage[i*Colomns+j],(int)(0.1*scale),(int)(0.087*scale));
                arrayOfImage[i*Colomns+j].setBackground(null);
                switch (arrayOfGame[i*Colomns+j]){
                    case 5:arrayOfImage[i*Colomns+j].setImageResource(R.drawable.mountain_nopl);break;
                    case 9:arrayOfImage[i*Colomns+j].setImageResource(R.drawable.river_nopl);break;
                    case 0:arrayOfImage[i*Colomns+j].setImageResource(R.drawable.card);break;

                }
                arrayOfImage[i*Colomns+j].setClickable(true);
                arrayOfImage[i*Colomns+j].setX((int) (0.078*j * scale));
                arrayOfImage[i*Colomns+j].setId(i*Colomns+j);
                arrayOfImage[i*Colomns+j].setScaleType(ImageView.ScaleType.FIT_XY);
                if (b==0){
                    arrayOfImage[i*Colomns+j].setY((int) (0.089*i * scale));
                    b=1;
                }
                else
                {
                    arrayOfImage[i*Colomns+j].setY((int) (0.089*i * scale+0.0435*scale));
                    b=0;
                }


                arrayOfImage[i*Colomns+j].setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        for (int i = 0; i < Rows; i++) {
                            for (int j = 0; j < Colomns; j++) {
                                if (arrayOfImage[i * Colomns + j].getId() == v.getId()) {
                                    if (event.getAction() == MotionEvent.ACTION_UP) {
                                        arrayOfImage[i * Colomns + j].setColorFilter(null);
                                        if (firsttop) {
                                            if (player == 1) {
                                                if (arrayOfGame[i * Colomns + j] == 0) {
                                                    addmycontrol(i, j);
                                                    player = 2;
                                                    arrayOfImage[i * Colomns + j].setImageResource(R.drawable.general_city_pl1);
                                                    arrayOfGame[i * Colomns + j] = 14;
                                                    playerrun.setImageResource(R.drawable.turn_red);
                                                    topforcity = true;
                                                    generalgreen[i * Colomns + j] = 3;
                                                    spawngreen = i * Colomns + j;
                                                }
                                            } else {
                                                if (arrayOfGame[i * Colomns + j] == 0) {
                                                    addmycontrol(i, j);
                                                    player = 1;
                                                    firsttop = false;
                                                    arrayOfImage[i * Colomns + j].setImageResource(R.drawable.general_city_pl2);
                                                    arrayOfGame[i * Colomns + j] = 24;
                                                    playerrun.setImageResource(R.drawable.turn_green);
                                                    generalred[i * Colomns + j] = 3;
                                                    spawnred = i * Colomns + j;
                                                }
                                            }
                                        } else {
                                            if (generalmove) {
                                                generalmovement(i, j);
                                                scan();
                                            }
                                            if (player == 1) {
                                                if (((arrayOfGame[i * Colomns + j] > 9) && (arrayOfGame[i * Colomns + j] < 16))||((arrayOfGame[i * Colomns + j] > 30) && (arrayOfGame[i * Colomns + j] < 34))) {
                                                    generalx = j;
                                                    generaly = i;
                                                    generalmove = true;
                                                }
                                            }
                                            if (player == 2) {
                                                if ((arrayOfGame[i * Colomns + j] > 19) && (arrayOfGame[i * Colomns + j] < 26)||((arrayOfGame[i * Colomns + j] > 40) && (arrayOfGame[i * Colomns + j] < 44))) {
                                                    generalx = j;
                                                    generaly = i;
                                                    generalmove = true;
                                                }
                                            }
                                        }
                                        if (Turns == 0) {
                                            Intent intent = new Intent(ActivityGameGenerals.this,Winner.class);
                                            intent.putExtra("arraygame",getIntent().getExtras().getIntArray("arraygame"));
                                            intent.putExtra("Turns",getIntent().getExtras().getInt("Turns"));
                                            intent.putExtra("Colomns",getIntent().getExtras().getInt("Colomns"));
                                            intent.putExtra("Rows",getIntent().getExtras().getInt("Rows"));
                                            intent.putExtra("Mode",3);
                                            if (!redwinner)
                                                intent.putExtra("Winner",1);
                                            else
                                                intent.putExtra("Winner",2);
                                            startActivity(intent);
                                        }
                                        break;

                                    }
                                }
                            }


                        }
                        return false;
                    }
                });
                arrayOfImage[i*Colomns+j].setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        for (int i = 0; i < Rows; i++) {
                            for (int j = 0; j < Colomns; j++) {
                                if (arrayOfImage[i * Colomns + j].getId() == v.getId()) {
                                    if (player == 1) {
                                        if (generalgreen[i*Colomns+j]>1) {
                                            if ((arrayOfGame[i * Colomns + j] == 11) || (arrayOfGame[i * Colomns + j] == 15) || (arrayOfGame[i * Colomns + j] == 13)) {
                                                if (youcontrolarround(i, j)) {
                                                    arrayOfImage[i * Colomns + j].setImageResource(R.drawable.castle_green);
                                                    arrayOfGame[i * Colomns + j] = 18;
                                                    addmycontrol(i, j);
                                                    generalgreen[i * Colomns + j] = 0;
                                                }
                                            }
                                        }
                                    }
                                    if (player == 2) {
                                        if (generalred[i*Colomns+j]>1) {
                                            if ((arrayOfGame[i * Colomns + j] == 21) || (arrayOfGame[i * Colomns + j] == 25) || (arrayOfGame[i * Colomns + j] == 23)) {
                                                if (youcontrolarround(i, j)) {
                                                    arrayOfImage[i * Colomns + j].setImageResource(R.drawable.castle_red);
                                                    arrayOfGame[i * Colomns + j] = 28;
                                                    addmycontrol(i, j);
                                                    generalred[i*Colomns+j]=0;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return false;
                    }
                });


            }
        }
    }
    public void endOfPartClick(View view){
        if (canendpart){
        if (player==1){ player=2; playerrun.setImageResource(R.drawable.turn_red);generalmove=false;generalred[spawnred]=3;generalreturn();arrayOfGame[spawnred]=24;arrayOfImage[spawnred].setImageResource(R.drawable.general_city_pl2);}
        else          { player=1; playerrun.setImageResource(R.drawable.turn_green);generalgreen[spawngreen]=3;generalreturn();arrayOfGame[spawngreen]=14;arrayOfImage[spawngreen].setImageResource(R.drawable.general_city_pl1);generalmove=false;Turns--;}
        canendpart=false;
        scan();}else
            Toast.makeText(ActivityGameGenerals.this,"You must remove your general from castle",Toast.LENGTH_SHORT).show();
    }
    public void onPauseClick(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityGameGenerals.this);
        builder.setTitle("Pause");
        builder.setMessage("Pause,for start game top start");
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Intent intent = new Intent(ActivityGameGenerals.this, MainActivity.class);
                startActivity(intent);
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Play", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
