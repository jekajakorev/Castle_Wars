package pixel.kostya.jenya.castles;


import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import static android.widget.ImageView.ScaleType.FIT_XY;
import static java.security.AccessController.getContext;


public class ActivityGame extends Activity {
    ImageView arrayOfImage[];
    int arrayOfGame[]={0};
    int Colomns,Rows,Turns;
    int player = 1;
    boolean redwinner=false;
    boolean firsttop = true;
    ImageView playerrun;
    TextView textred,textgreen;
    boolean topforcity = false;
    public boolean canput(int y,int x){
        if (player==1)
            {
                if (!(x % 2 == 0)) {
                    for (int i = 0; i < 2; i++) {
                        for (int j = 0; j < 3; j++) {
                            if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                                if ((arrayOfGame[(y + i) * Colomns + j - 1 + x] == 1) ||
                                        ((arrayOfGame[(y + i) * Colomns + j - 1 + x] > 10) && (arrayOfGame[(y + i) * Colomns + j - 1 + x] < 20))) {
                                    return true;
                                }
                        }
                    }
                    if (y - 1 >= 0)
                        if ((arrayOfGame[(y - 1) * Colomns + x] == 1) ||
                                ((arrayOfGame[(y - 1) * Colomns + x] > 10) && (arrayOfGame[(y - 1) * Colomns + x] < 20)))
                            return true;
                }
                if (!(x % 2 != 0)) {
                    for (int i = 0; i < 2; i++) {
                        for (int j = 0; j < 3; j++) {
                            if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                                if ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 1) ||
                                        ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] > 10) && (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] < 20))) {
                                    return true;
                                }
                        }
                    }
                    if (y + 1 < Rows)
                        if ((arrayOfGame[(y + 1) * Colomns + x] == 1) ||
                                ((arrayOfGame[(y + 1) * Colomns + x] > 10) && (arrayOfGame[(y + 1) * Colomns + x] < 20)))
                            return true;
                }
            }
            if (player == 2) {
                if (!(x % 2 == 0)) {
                    if (y - 1 >= 0)
                        if ((arrayOfGame[(y - 1) * Colomns + x] == 2) ||
                                ((arrayOfGame[(y - 1) * Colomns + x] > 25) && (arrayOfGame[(y - 1) * Colomns + x] < 30)))
                            return true;
                    for (int i = 0; i < 2; i++) {
                        for (int j = 0; j < 3; j++) {
                            if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                                if ((arrayOfGame[(y + i) * Colomns + j - 1 + x] == 2) ||
                                        ((arrayOfGame[(y + i) * Colomns + j - 1 + x] > 20) && (arrayOfGame[(y + i) * Colomns + j - 1 + x] < 30))) {
                                    return true;
                                }
                        }
                    }
                }


                if (!(x % 2 != 0)) {
                    for (int i = 0; i < 2; i++) {
                        for (int j = 0; j < 3; j++) {
                            if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                                if ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 2) ||
                                        ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] > 20) && (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] < 30))) {
                                    return true;
                                }
                        }
                    }
                    if (y + 1 < Rows)
                        if ((arrayOfGame[(y + 1) * Colomns + x] == 2) ||
                                ((arrayOfGame[(y + 1) * Colomns + x] > 20) && (arrayOfGame[(y + 1) * Colomns + x] < 30)))
                            return true;
                }
            }
        return false;
    }
    public void addmycontrol(int y,int x) {
        if (player==1) {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)) {
                            switch (arrayOfGame[(y + i) * Colomns + j - 1 + x]) {
                                case 4: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 5: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 28:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 16; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1red); break;
                                case 26:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 18; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1); break;
                                case 29:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 9: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 27:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 0:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                            }
                        }
                    }
                }
                if (y - 1 >= 0){
                    switch (arrayOfGame[(y - 1) * Colomns + x]) {
                        case 4: arrayOfGame[(y - 1) * Colomns + x] = 3; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 5: arrayOfGame[(y - 1) * Colomns + x] = 3; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 28:arrayOfGame[(y - 1) * Colomns + x] = 16; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl1red); break;
                        case 26:arrayOfGame[(y - 1) * Colomns + x] = 18; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl1); break;
                        case 29:arrayOfGame[(y - 1) * Colomns + x] = 19; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 9: arrayOfGame[(y - 1) * Colomns + x] = 19; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 27:arrayOfGame[(y - 1) * Colomns + x] = 17; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 0:arrayOfGame[(y - 1) * Colomns + x] = 17; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                    }
                }

            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)){
                            switch (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x])  {
                                case 4: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 5: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 28:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 16; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1red); break;
                                case 26:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 18; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1); break;
                                case 29:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 9: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 27:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 0:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                            }
                        }
                    }
                }
                if (y + 1 < Rows)
                {
                    switch (arrayOfGame[(y + 1) * Colomns + x]){
                        case 4: arrayOfGame[(y + 1) * Colomns + x] = 3; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 5: arrayOfGame[(y + 1) * Colomns + x] = 3; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 28:arrayOfGame[(y + 1) * Colomns + x] = 16; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl1red); break;
                        case 26:arrayOfGame[(y + 1) * Colomns + x] = 18; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl1); break;
                        case 29:arrayOfGame[(y + 1) * Colomns + x] = 19; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 9: arrayOfGame[(y + 1) * Colomns + x] = 19; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 27:arrayOfGame[(y + 1) * Colomns + x] = 17; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 0:arrayOfGame[(y + 1) * Colomns + x] = 17; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;

                    }
                }

            }

        }
        if (player==2) {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)) {
                            switch (arrayOfGame[(y + i) * Colomns + j - 1 + x]) {
                                case 3: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 5: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 18:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 26; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2green); break;
                                case 16:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 28; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2); break;
                                case 19:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 9: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 17:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 0:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                            }
                        }
                    }
                }
                if (y - 1 >= 0){
                    switch (arrayOfGame[(y - 1) * Colomns + x]) {
                        case 3: arrayOfGame[(y - 1) * Colomns + x] = 4; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 5: arrayOfGame[(y - 1) * Colomns + x] = 4; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 18:arrayOfGame[(y - 1) * Colomns + x] = 26; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl2green); break;
                        case 16:arrayOfGame[(y - 1) * Colomns + x] = 28; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl2); break;
                        case 19:arrayOfGame[(y - 1) * Colomns + x] = 29; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 9: arrayOfGame[(y - 1) * Colomns + x] = 29; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 17:arrayOfGame[(y - 1) * Colomns + x] = 27; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 0:arrayOfGame[(y - 1) * Colomns + x] = 27; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                    }
                }

            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)){
                            switch (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x])  {
                                case 3: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 5: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 18:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 26; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2green); break;
                                case 16:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 28; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2); break;
                                case 19:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 9: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 17:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 0:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                            }
                        }
                    }
                }
                if (y + 1 < Rows)
                {
                    switch (arrayOfGame[(y + 1) * Colomns + x]){
                        case 3: arrayOfGame[(y + 1) * Colomns + x] = 4; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 5: arrayOfGame[(y + 1) * Colomns + x] = 4; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 18:arrayOfGame[(y + 1) * Colomns + x] = 26; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl2green); break;
                        case 16:arrayOfGame[(y + 1) * Colomns + x] = 28; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl2); break;
                        case 19:arrayOfGame[(y + 1) * Colomns + x] = 29; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 9: arrayOfGame[(y + 1) * Colomns + x] = 29; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 17:arrayOfGame[(y + 1) * Colomns + x] = 27; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 0:arrayOfGame[(y + 1) * Colomns + x] = 27; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;

                    }
                }

            }

        }






    }

    public void addmycastle (int y,int x) {
        TextView textView = (TextView)findViewById(R.id.textView2);
        if ((arrayOfGame[y * Colomns + x] == 0)||
        (arrayOfGame[y * Colomns + x] == 17) || (arrayOfGame[y * Colomns + x] == 27)){
        {
            if (canput(y, x)) {
                if (player == 1) {
                    arrayOfGame[y * Colomns + x] = 18;
                    arrayOfImage[y * Colomns + x].setImageResource(R.drawable.castle_pl1);
                    addmycontrol(y,x);
                    player = 2;
                    playerrun.setImageResource(R.drawable.turn_red);
                }
                else{
                    arrayOfGame[y * Colomns + x] = 28;
                    arrayOfImage[y * Colomns + x].setImageResource(R.drawable.castle_pl2);
                    addmycontrol(y,x);
                    player=1;
                    playerrun.setImageResource(R.drawable.turn_green);
                    Turns--;
                    textView.setText(Integer.toString(Turns));
                }
            }
        }
    }
    }
    public boolean cantput(){
        for (int i=0;i<Rows;i++)
            for(int j=0;j<Colomns;j++) {
                if ((arrayOfGame[i * Colomns + j] == 0)||
                        (arrayOfGame[i * Colomns + j] == 17) || (arrayOfGame[i * Colomns + j] == 27))
                    if (canput(i, j))
                        return false;
            }
        return true;
    }
    public void scan(){
        int playero=0;
        int playert=0;
        int count=0;
        for (int i=0;i<Rows;i++)
            for (int j=0;j<Colomns;j++){
                if ((arrayOfGame[i*Colomns+j]==1)||(arrayOfGame[i*Colomns+j]==3))//(arrayOfGame[i*Colomns+j]==18)||(arrayOfGame[i*Colomns+j]==19)||(arrayOfGame[i*Colomns+j]==17))
                    playero=playero+1;
                if ((arrayOfGame[i*Colomns+j]==2)||(arrayOfGame[i*Colomns+j]==4))//||(arrayOfGame[i*Colomns+j]==28)||(arrayOfGame[i*Colomns+j]==29)||(arrayOfGame[i*Colomns+j]==27))
                    playert++;
                if ((arrayOfGame[i*Colomns+j]>15)&&(arrayOfGame[i*Colomns+j]<20))
                    playero++;
                if ((arrayOfGame[i*Colomns+j]>25)&&(arrayOfGame[i*Colomns+j]<30))
                    playert++;
            }
            textgreen.setText(Integer.toString(playero));
            textred.setText(Integer.toString(playert));
            TextView textView = (TextView)findViewById(R.id.textView2);
            textView.setText(Integer.toString(Turns));


        if (playero<playert)
            redwinner=true;
        else
            redwinner=false;
    }
    public boolean scanvoid(){
        int a=0;
        for (int i=0;i<Rows;i++) {
            for (int j = 0; j < Colomns; j++) {
                if (arrayOfGame[i * Colomns + j] == 0)
                    a++;
                if (a > 2)
                    return true;
            }
        }
        return false;

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Button button = (Button) findViewById(R.id.button2);
        button.setVisibility(View.GONE);
        playerrun = (ImageView) findViewById(R.id.playerrun);
        textred = (TextView) findViewById(R.id.textView);
        textgreen = (TextView) findViewById(R.id.textView7);
        Colomns = getIntent().getExtras().getInt("Colomns");
        Rows = getIntent().getExtras().getInt("Rows");
        Turns = getIntent().getExtras().getInt("Turns");
        arrayOfGame = getIntent().getExtras().getIntArray("arraygame");
        arrayOfImage= new ImageView[Colomns*Rows];
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(Integer.toString(Turns));

        topforcity = false;



        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int scalex = metrics.widthPixels;
        int scaley = metrics.heightPixels;
        float dp = metrics.density;
        int scale;
        if (scalex < scaley) scale = scaley;
        else scale = scalex;
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView);
        RelativeLayout flay = new RelativeLayout(getApplicationContext());
        int height=(int)(scale*Rows*0.089+0.0435*scale);
        int width=(int)(0.078*Colomns * scale+0.025*scale);
        scrollView.addView(flay,width,height);
//        Log.d("ImageView3:", "Width"+(((ImageView)findViewById(R.id.ImageView3)).getWidth()));
        if (width < scalex-119*dp-106*dp) {
            scrollView.setX((int) (((scalex-width-119*dp-106*dp)/2)));}
        if (height < scaley){
            scrollView.setY((int)((scaley-height)/2));}

        /*if (scale * Colomns * 0.074 <= scalex - scale * 0.08*2) {

            lparams.width = (int) (scale * Colomns * 0.08);
            //lparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        } //else{
            //lparams.addRule(RelativeLayout.RIGHT_OF,R.id.ImageView3);
            //lparams.addRule(RelativeLayout.ALIGN_TOP,R.id.playerrun);
            //lparams.addRule(RelativeLayout.START_OF,R.id.playerrun);
            //lparams.addRule(RelativeLayout.END_OF,R.id.imageView);
        //}
        if (scale*Rows*0.087+0.0435*scale <= scaley){
            lparams.height = (int)(scale*Rows*0.087+0.0435*scale);
            //lparams.addRule(RelativeLayout.CENTER_VERTICAL);
        }
        scrollView.setLayoutParams(lparams);*/

        for (int i=0;i<Rows;i++){
            int b=0;
            for (int j=0;j<Colomns;j++){
                arrayOfImage[i*Colomns+j] = new ImageView(getApplicationContext());
                flay.addView(arrayOfImage[i*Colomns+j],(int)(0.1*scale),(int)(0.087*scale));
                arrayOfImage[i*Colomns+j].setBackground(null);
                switch (arrayOfGame[i*Colomns+j]){
                    case 5:arrayOfImage[i*Colomns+j].setImageResource(R.drawable.mountain_nopl);break;
                    case 9:arrayOfImage[i*Colomns+j].setImageResource(R.drawable.river_nopl);break;
                    case 0:arrayOfImage[i*Colomns+j].setImageResource(R.drawable.card);break;

                }
                arrayOfImage[i*Colomns+j].setClickable(true);
                arrayOfImage[i*Colomns+j].setX((int) (0.078*j * scale));
                arrayOfImage[i*Colomns+j].setId(i*Colomns+j);
                arrayOfImage[i*Colomns+j].setScaleType(ImageView.ScaleType.FIT_XY);
                if (b==0){
                    arrayOfImage[i*Colomns+j].setY((int) (0.089*i * scale));
                    b=1;
                }
                else
                {
                    arrayOfImage[i*Colomns+j].setY((int) (0.089*i * scale+0.0435*scale));
                    b=0;
                }
                arrayOfImage[i*Colomns+j].setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        for (int i = 0; i < Rows; i++) {
                            for (int j = 0; j < Colomns; j++) {
                                if (arrayOfImage[i * Colomns + j].getId() == v.getId()) {
                                    if (event.getAction() == MotionEvent.ACTION_UP) {
                                        arrayOfImage[i * Colomns + j].setColorFilter(null);
                                        if (player == 1) {
                                            if (firsttop) {
                                                if (arrayOfGame[i * Colomns + j] == 0) {
                                                    arrayOfImage[i * Colomns + j].setImageResource(R.drawable.city_pl1);
                                                    arrayOfGame[i * Colomns + j] = 1;
                                                    addmycontrol(i, j);
                                                    player = 2;
                                                    playerrun.setImageResource(R.drawable.turn_red);
                                                    topforcity = true;
                                                    scan();
                                                }
                                            } else {
                                                if (Turns != 0)
                                                    addmycastle(i, j);
                                                scan();
                                            }
                                        } else {
                                            if (firsttop) {
                                                if (arrayOfGame[i * Colomns + j] == 0) {
                                                    firsttop = false;
                                                    arrayOfImage[i * Colomns + j].setImageResource(R.drawable.city_pl2);
                                                    arrayOfGame[i * Colomns + j] = 2;
                                                    addmycontrol(i, j);
                                                    player = 1;
                                                    playerrun.setImageResource(R.drawable.turn_green);
                                                    scan();
                                                }
                                            } else {
                                                if (Turns != 0)
                                                    addmycastle(i, j);
                                                scan();
                                            }
                                        }
                                        ;
                                        if ((Turns == 0) || ((cantput()) && (!firsttop))) {
                                            Intent intent = new Intent(ActivityGame.this,Winner.class);
                                            intent.putExtra("arraygame",getIntent().getExtras().getIntArray("arraygame"));
                                            intent.putExtra("Turns",getIntent().getExtras().getInt("Turns"));
                                            intent.putExtra("Colomns",getIntent().getExtras().getInt("Colomns"));
                                            intent.putExtra("Rows",getIntent().getExtras().getInt("Rows"));
                                            intent.putExtra("Mode",1);
                                            if (!redwinner)
                                                intent.putExtra("Winner",1);
                                            else
                                                intent.putExtra("Winner",2);
                                            startActivity(intent);
                                        }
                                    }

                                }
                            }
                        }

                        return false;
                    }

                });

            }
        }


    }
    public void onPauseClick(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityGame.this);
        builder.setTitle("Pause");
        builder.setMessage("Pause,for start game top start");
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Intent intent = new Intent(ActivityGame.this, MainActivity.class);
                startActivity(intent);
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Play", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


}
