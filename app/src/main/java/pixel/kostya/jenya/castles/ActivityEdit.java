package pixel.kostya.jenya.castles;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

/**
 * Created by student on 19.10.2016.
 */

public class ActivityEdit extends Activity{
    int arrayOfGame[];
    ImageView arrayOfImage[];
    int Colomns, Rows;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Colomns = getIntent().getExtras().getInt("Colomns");
        Rows = getIntent().getExtras().getInt("Rows");
        arrayOfImage = new ImageView[Colomns * Rows];
        arrayOfGame = new int[Colomns*Rows];


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int scalex = metrics.widthPixels;
        int scaley = metrics.heightPixels;
        float dp = metrics.density;
        int scale;
        if (scalex < scaley) scale = scaley;
        else scale = scalex;
        HorizontalScrollView horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView);
        RelativeLayout flay = new RelativeLayout(getApplicationContext());
        int height=(int)(scale*Rows*0.089+0.0435*scale);
        int width=(int)(0.078*Colomns * scale+0.025*scale);
        scrollView.addView(flay,width,height);
//        Log.d("ImageView3:", "Width"+(((ImageView)findViewById(R.id.ImageView3)).getWidth()));
        if (width < scalex-119*dp-106*dp) {
            scrollView.setX((int) (((scalex-width-119*dp-106*dp)/2)));}
        if (height < scaley){
            scrollView.setY((int)((scaley-height)/2));}

        /*if (scale * Colomns * 0.074 <= scalex - scale * 0.08*2) {

            lparams.width = (int) (scale * Colomns * 0.08);
            //lparams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        } //else{
            //lparams.addRule(RelativeLayout.RIGHT_OF,R.id.ImageView3);
            //lparams.addRule(RelativeLayout.ALIGN_TOP,R.id.playerrun);
            //lparams.addRule(RelativeLayout.START_OF,R.id.playerrun);
            //lparams.addRule(RelativeLayout.END_OF,R.id.imageView);
        //}
        if (scale*Rows*0.087+0.0435*scale <= scaley){
            lparams.height = (int)(scale*Rows*0.087+0.0435*scale);
            //lparams.addRule(RelativeLayout.CENTER_VERTICAL);
        }
        scrollView.setLayoutParams(lparams);*/

        for (int i=0;i<Rows;i++){
            int b=0;
            for (int j=0;j<Colomns;j++){
                arrayOfImage[i*Colomns+j] = new ImageView(getApplicationContext());
                flay.addView(arrayOfImage[i*Colomns+j],(int)(0.1*scale),(int)(0.087*scale));
                arrayOfImage[i*Colomns+j].setBackground(null);
                arrayOfImage[i*Colomns+j].setImageResource(R.drawable.card);
                arrayOfImage[i*Colomns+j].setClickable(true);
                arrayOfImage[i*Colomns+j].setX((int) (0.078*j * scale));
                arrayOfImage[i*Colomns+j].setId(i*Colomns+j);
                arrayOfImage[i*Colomns+j].setScaleType(ImageView.ScaleType.FIT_XY);
                if (b==0){
                    arrayOfImage[i*Colomns+j].setY((int) (0.089*i * scale));
                    b=1;
                }
                else
                {
                    arrayOfImage[i*Colomns+j].setY((int) (0.089*i * scale+0.0445*scale));
                    b=0;
                }
                arrayOfImage[i*Colomns+j].setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        for (int i = 0; i < Rows; i++) {
                            for (int j = 0; j < Colomns; j++) {
                                if (arrayOfImage[i * Colomns + j].getId() == v.getId()) {
                                    if (event.getAction() == MotionEvent.ACTION_UP) {
                                        arrayOfImage[i * Colomns + j].setColorFilter(null);
                                        switch (arrayOfGame[i * Colomns + j]) {
                                            case 0:
                                                arrayOfGame[i * Colomns + j] = 5;
                                                arrayOfImage[i * Colomns + j].setImageResource(R.drawable.mountain_nopl);
                                                break;
                                            case 5:
                                                arrayOfGame[i * Colomns + j] = 9;
                                                arrayOfImage[i * Colomns + j].setImageResource(R.drawable.river_nopl);
                                                break;
                                            case 9:
                                                arrayOfGame[i * Colomns + j] = 0;
                                                arrayOfImage[i * Colomns + j].setImageResource(R.drawable.card);
                                                break;
                                        }
                                    }
                                }
                            }
                        } return false;
                    }
                });
            }
        }
    }
    public void onPauseClick(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityEdit.this);
        builder.setTitle("Pause");
        builder.setMessage("Pause,for start game top start");
        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Intent intent = new Intent(ActivityEdit.this, MainActivity.class);
                startActivity(intent);
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Play", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void onStartClick(View view){
        switch (getIntent().getExtras().getInt("Mode")) {
            case 1:Intent intent = new Intent(ActivityEdit.this, ActivityGame.class);
                intent.putExtra("Colomns", Colomns);
                intent.putExtra("Rows", Rows);
                intent.putExtra("Turns", getIntent().getExtras().getInt("Turns"));
                intent.putExtra("arraygame", arrayOfGame);
                startActivity(intent);
            break;
            case 2:
                if (getIntent().getExtras().getInt("Team")==1) intent = new Intent(ActivityEdit.this, ActivityGameBot.class);
                else intent = new Intent(ActivityEdit.this, ActivityGameBotRedFirst.class);
                intent.putExtra("Colomns", Colomns);
                intent.putExtra("Rows", Rows);
                intent.putExtra("Turns", getIntent().getExtras().getInt("Turns"));
                intent.putExtra("arraygame", arrayOfGame);
                startActivity(intent);
                break;
            case 3:intent = new Intent(ActivityEdit.this, ActivityGameGenerals.class);
                intent.putExtra("Colomns", Colomns);
                intent.putExtra("Rows", Rows);
                intent.putExtra("Turns", getIntent().getExtras().getInt("Turns"));
                intent.putExtra("arraygame", arrayOfGame);
                startActivity(intent);
                break;
        }

    }
}
