package pixel.kostya.jenya.castles;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;


public class ActivitySecond extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_active);
        if (getIntent().getExtras().getInt("Team")==2) {
            ((ConstraintLayout) findViewById(R.id.second_activity)).setBackgroundResource(R.drawable.activity_options_red);
            ((Button)findViewById(R.id.button3)).setBackgroundResource(R.drawable.player_vs_androidgreen);
        }
    }
    public void player_vs_player(View view){
        Intent intent = new Intent(ActivitySecond.this, ActivityThird.class);
        intent.putExtra("Game Mode",0);
        intent.putExtra("Team",getIntent().getExtras().getInt("Team"));
        startActivity(intent);
    }
    public void player_vs_android(View view){
        Intent intent = new Intent(ActivitySecond.this, ActivityThird.class);
        intent.putExtra("Game Mode",1);
        intent.putExtra("Team",getIntent().getExtras().getInt("Team"));
        startActivity(intent);
    }
    public void read_from_file(View view){

        Intent intent = new Intent(ActivitySecond.this,ReadFromFile.class);
        startActivity(intent);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent(ActivitySecond.this,ReadFromFile.class);
        intent.putExtra("name",data.getData());
        startActivity(intent);
    }
    public void onBackTop(View view){
        Intent intent = new Intent(ActivitySecond.this, MainActivity.class);
        startActivity(intent);
    }


}
