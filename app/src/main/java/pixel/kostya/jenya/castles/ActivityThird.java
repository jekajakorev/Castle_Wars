package pixel.kostya.jenya.castles;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.w3c.dom.Text;

/**
 * Created by Jenya on 07.10.2016.
 */
public class ActivityThird extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third_activity);
        if (getIntent().getExtras().getInt("Team")==2)
            ((RelativeLayout)findViewById(R.id.third_activity)).setBackgroundResource(R.drawable.activity_options_red);
        SeekBar seekBar = (SeekBar)findViewById(R.id.colomnsseek);
        if (getIntent().getExtras().getInt("Game Mode")==1){
        //((TextViewPlus)findViewById(R.id.textView13)).setVisibility(View.GONE);
        //((SwitchCompat)findViewById(R.id.switchCompat)).setVisibility(View.GONE);
        }
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SeekBar colomns = (SeekBar)findViewById(R.id.colomnsseek);
                SeekBar rows = (SeekBar)findViewById(R.id.rowsseek);
                SeekBar turns = (SeekBar)findViewById(R.id.turnsseek);
                TextView colomnstext = (TextView)findViewById(R.id.textView11);
                TextView rowstext = (TextView)findViewById(R.id.textView12);
                TextView turnstext = (TextView)findViewById(R.id.textView9);
                turns.setMax((colomns.getProgress()+5)*(rows.getProgress()+5)/2-4);
                turns.setProgress((colomns.getProgress()+5)*(rows.getProgress()+5)/2-4);
                colomnstext.setText(Integer.toString(colomns.getProgress()+5));
                turnstext.setText(Integer.toString(turns.getProgress()+5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar = (SeekBar)findViewById(R.id.rowsseek);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SeekBar colomns = (SeekBar)findViewById(R.id.colomnsseek);
                SeekBar rows = (SeekBar)findViewById(R.id.rowsseek);
                SeekBar turns = (SeekBar)findViewById(R.id.turnsseek);
                TextView rowstext = (TextView)findViewById(R.id.textView12);
                TextView turnstext = (TextView)findViewById(R.id.textView9);
                turns.setMax((colomns.getProgress()+5)*(rows.getProgress()+5)/2-4);
                turns.setProgress((colomns.getProgress()+5)*(rows.getProgress()+5)/2-4);
                rowstext.setText(Integer.toString(rows.getProgress()+5));
                turnstext.setText(Integer.toString(turns.getProgress()+5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar = (SeekBar)findViewById(R.id.turnsseek);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SeekBar turns = (SeekBar)findViewById(R.id.turnsseek);
                TextView turnstext = (TextView)findViewById(R.id.textView9);
                turnstext.setText(Integer.toString(turns.getProgress()+5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
    public void onStartButtonClick2(View view){
        SeekBar colomns = (SeekBar)findViewById(R.id.colomnsseek);
        SeekBar rows = (SeekBar)findViewById(R.id.rowsseek);
        SeekBar turns = (SeekBar)findViewById(R.id.turnsseek);
        //SwitchCompat radioButton = (SwitchCompat) findViewById(R.id.switchCompat);
        SwitchCompat switchCompat = (SwitchCompat)findViewById(R.id.switch1);

        switch (getIntent().getExtras().getInt("Game Mode")){
            case 0: if (false){
                //---------GENERALS-------------------------
                if (switchCompat.isChecked()==true) {
                    Intent intent = new Intent(ActivityThird.this, ActivityEdit.class);
                    intent.putExtra("Colomns", colomns.getProgress() + 5);
                    intent.putExtra("Rows", rows.getProgress() + 5);
                    intent.putExtra("Turns", turns.getProgress() + 5);
                    intent.putExtra("Mode",3);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(ActivityThird.this, ActivityGameGenerals.class);
                    intent.putExtra("Colomns", colomns.getProgress() + 5);
                    intent.putExtra("Rows", rows.getProgress() + 5);
                    intent.putExtra("Turns", turns.getProgress() + 5);
                    intent.putExtra("arraygame",new int[(rows.getProgress()+5)*(colomns.getProgress()+5)]);
                    startActivity(intent);
                }
                //---------GENERALS-------------------------
                //---------GAME-------------------------
            }else{
                if (switchCompat.isChecked()==true) {
                    Intent intent = new Intent(ActivityThird.this, ActivityEdit.class);
                    intent.putExtra("Colomns", colomns.getProgress() + 5);
                    intent.putExtra("Rows", rows.getProgress() + 5);
                    intent.putExtra("Turns", turns.getProgress() + 5);
                    intent.putExtra("Mode",1);
                    startActivity(intent);
                }
                else{
                    Intent intent = new Intent(ActivityThird.this, ActivityGame.class);
                    intent.putExtra("Colomns", colomns.getProgress() + 5);
                    intent.putExtra("Rows", rows.getProgress() + 5);
                    intent.putExtra("Turns", turns.getProgress() + 5);
                    intent.putExtra("arraygame",new int[(rows.getProgress()+5)*(colomns.getProgress()+5)]);
                    startActivity(intent);
                }
                } break;
            //---------GAME-------------------------
            //------------------ANDROID-------------
            case 1:if (!(false)) {
                Intent intent;
                if (switchCompat.isChecked() == true) {
                    intent = new Intent(ActivityThird.this, ActivityEdit.class);
                    intent.putExtra("Team",getIntent().getExtras().getInt("Team"));
                    intent.putExtra("Colomns", colomns.getProgress() + 5);
                    intent.putExtra("Rows", rows.getProgress() + 5);
                    intent.putExtra("Turns", turns.getProgress() + 5);
                    intent.putExtra("Mode", 2);
                    startActivity(intent);
                } else {
                    if (getIntent().getExtras().getInt("Team")==1)
                            intent= new Intent(ActivityThird.this, ActivityGameBot.class);
                    else
                        intent = new Intent(ActivityThird.this, ActivityGameBotRedFirst.class);
                    intent.putExtra("Colomns", colomns.getProgress() + 5);
                    intent.putExtra("Rows", rows.getProgress() + 5);
                    intent.putExtra("Turns", turns.getProgress() + 5);
                    intent.putExtra("arraygame", new int[(rows.getProgress() + 5) * (colomns.getProgress() + 5)]);
                    startActivity(intent);
                }
            }
                break;
                //------------------ANDROID-------------
        }

    }
    public void onBackTop(View view){
        Intent intent = new Intent(ActivityThird.this, ActivitySecond.class);
        intent.putExtra("Team",getIntent().getExtras().getInt("Team"));
        startActivity(intent);
    }

}
