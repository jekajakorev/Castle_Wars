package pixel.kostya.jenya.castles;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    double scalex,scaley,k;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        scalex = metrics.widthPixels;
        scaley = metrics.heightPixels;
        ((ConstraintLayout)findViewById(R.id.MainActivityTop)).setOnTouchListener(new View.OnTouchListener() {
            @Override
        public boolean onTouch(View v, MotionEvent event) {
                k = scaley / scalex * (-1);
                double x = event.getX();
                double y = event.getY();
                if (y < (k * x + scaley)) {
                    Intent intent = new Intent(MainActivity.this, ActivitySecond.class);
                    intent.putExtra("Team", 1);
                    startActivity(intent);
                }
                if (y > (k * x + scaley)) {
                    Intent intent = new Intent(MainActivity.this, ActivitySecond.class);
                    intent.putExtra("Team", 2);
                    startActivity(intent);
                }
                return true;
            }
        });
    }


    public void onMainActivityTop(View view){

    }
//    public void onGreenClick(View view){
//        Intent intent = new Intent(MainActivity.this,ActivitySecond.class);
//        intent.putExtra("Team",1);
//        startActivity(intent);
//    }
//    public void onRedClick(View view){
//        Intent intent = new Intent(MainActivity.this,ActivitySecond.class);
//        intent.putExtra("Team",2);
//        startActivity(intent);
//    }
}



