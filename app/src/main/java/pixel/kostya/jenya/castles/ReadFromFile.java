package pixel.kostya.jenya.castles;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresPermission;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewDebug;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
public class ReadFromFile extends MainActivity {
    ImageButton arrayOfImage[];
    int arrayOfGame[];
    int Colomns,Rows,Turns;
    int player = 1;
    boolean redwinner=false;
    boolean firsttop = true;
    ImageButton playerrun;
    TextView text;
    boolean topforcity = false;
    public boolean canput(int y,int x){
        if (player==1)
        {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i) * Colomns + j - 1 + x] == 1) ||
                                    ((arrayOfGame[(y + i) * Colomns + j - 1 + x] > 10) && (arrayOfGame[(y + i) * Colomns + j - 1 + x] < 20))) {
                                return true;
                            }
                    }
                }
                if (y - 1 >= 0)
                    if ((arrayOfGame[(y - 1) * Colomns + x] == 1) ||
                            ((arrayOfGame[(y - 1) * Colomns + x] > 10) && (arrayOfGame[(y - 1) * Colomns + x] < 20)))
                        return true;
            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 1) ||
                                    ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] > 10) && (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] < 20))) {
                                return true;
                            }
                    }
                }
                if (y + 1 < Rows)
                    if ((arrayOfGame[(y + 1) * Colomns + x] == 1) ||
                            ((arrayOfGame[(y + 1) * Colomns + x] > 10) && (arrayOfGame[(y + 1) * Colomns + x] < 20)))
                        return true;
            }
        }
        if (player == 2) {
            if (!(x % 2 == 0)) {
                if (y - 1 >= 0)
                    if ((arrayOfGame[(y - 1) * Colomns + x] == 2) ||
                            ((arrayOfGame[(y - 1) * Colomns + x] > 25) && (arrayOfGame[(y - 1) * Colomns + x] < 30)))
                        return true;
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i) * Colomns + j - 1 + x] == 2) ||
                                    ((arrayOfGame[(y + i) * Colomns + j - 1 + x] > 20) && (arrayOfGame[(y + i) * Colomns + j - 1 + x] < 30))) {
                                return true;
                            }
                    }
                }
            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns))
                            if ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] == 2) ||
                                    ((arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] > 20) && (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] < 30))) {
                                return true;
                            }
                    }
                }
                if (y + 1 < Rows)
                    if ((arrayOfGame[(y + 1) * Colomns + x] == 2) ||
                            ((arrayOfGame[(y + 1) * Colomns + x] > 20) && (arrayOfGame[(y + 1) * Colomns + x] < 30)))
                        return true;
            }
        }
        return false;
    }
    public void addmycontrol(int y,int x) {
        if (player==1) {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)) {
                            switch (arrayOfGame[(y + i) * Colomns + j - 1 + x]) {
                                case 4: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 5: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 28:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 16; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1red); break;
                                case 26:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 18; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1); break;
                                case 29:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 9: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 27:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 0:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                            }
                        }
                    }
                }
                if (y - 1 >= 0){
                    switch (arrayOfGame[(y - 1) * Colomns + x]) {
                        case 4: arrayOfGame[(y - 1) * Colomns + x] = 3; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 5: arrayOfGame[(y - 1) * Colomns + x] = 3; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 28:arrayOfGame[(y - 1) * Colomns + x] = 16; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl1red); break;
                        case 26:arrayOfGame[(y - 1) * Colomns + x] = 18; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl1); break;
                        case 29:arrayOfGame[(y - 1) * Colomns + x] = 19; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 9: arrayOfGame[(y - 1) * Colomns + x] = 19; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 27:arrayOfGame[(y - 1) * Colomns + x] = 17; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 0:arrayOfGame[(y - 1) * Colomns + x] = 17; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                    }
                }
            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)){
                            switch (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x])  {
                                case 4: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 5: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 3; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl1); break;
                                case 28:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 16; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1red); break;
                                case 26:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 18; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl1); break;
                                case 29:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 9: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 19; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl1); break;
                                case 27:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                                case 0:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 17; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl1); break;
                            }
                        }
                    }
                }
                if (y + 1 < Rows)
                {
                    switch (arrayOfGame[(y + 1) * Colomns + x]){
                        case 4: arrayOfGame[(y + 1) * Colomns + x] = 3; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 5: arrayOfGame[(y + 1) * Colomns + x] = 3; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl1); break;
                        case 28:arrayOfGame[(y + 1) * Colomns + x] = 16; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl1red); break;
                        case 26:arrayOfGame[(y + 1) * Colomns + x] = 18; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl1); break;
                        case 29:arrayOfGame[(y + 1) * Colomns + x] = 19; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 9: arrayOfGame[(y + 1) * Colomns + x] = 19; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl1); break;
                        case 27:arrayOfGame[(y + 1) * Colomns + x] = 17; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                        case 0:arrayOfGame[(y + 1) * Colomns + x] = 17; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl1); break;
                    }
                }
            }
        }
        if (player==2) {
            if (!(x % 2 == 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i < Rows) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)) {
                            switch (arrayOfGame[(y + i) * Colomns + j - 1 + x]) {
                                case 3: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 5: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 18:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 26; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2green); break;
                                case 16:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 28; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2); break;
                                case 19:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 9: arrayOfGame[(y + i) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 17:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 0:arrayOfGame[(y + i) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                            }
                        }
                    }
                }
                if (y - 1 >= 0){
                    switch (arrayOfGame[(y - 1) * Colomns + x]) {
                        case 3: arrayOfGame[(y - 1) * Colomns + x] = 4; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 5: arrayOfGame[(y - 1) * Colomns + x] = 4; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 18:arrayOfGame[(y - 1) * Colomns + x] = 26; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl2green); break;
                        case 16:arrayOfGame[(y - 1) * Colomns + x] = 28; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.castle_pl2); break;
                        case 19:arrayOfGame[(y - 1) * Colomns + x] = 29; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 9: arrayOfGame[(y - 1) * Colomns + x] = 29; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 17:arrayOfGame[(y - 1) * Colomns + x] = 27; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 0:arrayOfGame[(y - 1) * Colomns + x] = 27; arrayOfImage[(y - 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                    }
                }
            }
            if (!(x % 2 != 0)) {
                for (int i = 0; i < 2; i++) {
                    for (int j = 0; j < 3; j++) {
                        if ((y + i - 1 >= 0) && (j - 1 + x >= 0) && (j - 1 + x < Colomns)){
                            switch (arrayOfGame[(y + i - 1) * Colomns + j - 1 + x])  {
                                case 3: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 5: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 4; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.mountain_pl2); break;
                                case 18:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 26; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2green); break;
                                case 16:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 28; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.castle_pl2); break;
                                case 19:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 9: arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 29; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.river_pl2); break;
                                case 17:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                                case 0:arrayOfGame[(y + i - 1) * Colomns + j - 1 + x] = 27; arrayOfImage[(y + i - 1) * Colomns + j - 1 + x].setImageResource(R.drawable.card_pl2); break;
                            }
                        }
                    }
                }
                if (y + 1 < Rows)
                {
                    switch (arrayOfGame[(y + 1) * Colomns + x]){
                        case 3: arrayOfGame[(y + 1) * Colomns + x] = 4; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 5: arrayOfGame[(y + 1) * Colomns + x] = 4; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.mountain_pl2); break;
                        case 18:arrayOfGame[(y + 1) * Colomns + x] = 26; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl2green); break;
                        case 16:arrayOfGame[(y + 1) * Colomns + x] = 28; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.castle_pl2); break;
                        case 19:arrayOfGame[(y + 1) * Colomns + x] = 29; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 9: arrayOfGame[(y + 1) * Colomns + x] = 29; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.river_pl2); break;
                        case 17:arrayOfGame[(y + 1) * Colomns + x] = 27; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                        case 0:arrayOfGame[(y + 1) * Colomns + x] = 27; arrayOfImage[(y + 1) * Colomns + x].setImageResource(R.drawable.card_pl2); break;
                    }
                }
            }
        }
    }
    public void addmycastle (int y,int x) {
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setVisibility(View.INVISIBLE);
        if ((arrayOfGame[y * Colomns + x] == 0)||
                (arrayOfGame[y * Colomns + x] == 17) || (arrayOfGame[y * Colomns + x] == 27)){
            {
                if (canput(y, x)) {
                    if (player == 1) {
                        arrayOfGame[y * Colomns + x] = 18;
                        arrayOfImage[y * Colomns + x].setImageResource(R.drawable.castle_pl1);
                        addmycontrol(y,x);
                        player = 2;
                        playerrun.setImageResource(R.drawable.card_pl2);
                    }
                    else{
                        arrayOfGame[y * Colomns + x] = 28;
                        arrayOfImage[y * Colomns + x].setImageResource(R.drawable.castle_pl2);
                        addmycontrol(y,x);
                        player=1;
                        playerrun.setImageResource(R.drawable.card_pl1);
                        textView.setText("Turns Left: "+Integer.toString(Turns));
                    }
                }
            }
        }else {
            Intent intent = new Intent(ReadFromFile.this,MainActivity.class);
            Toast.makeText(ReadFromFile.this,"Can't do smth",Toast.LENGTH_SHORT).show();
            startActivity(intent);
        }
    }
    public void scan(){
        int playero=0;
        int playert=0;
        int count=0;
        for (int i=0;i<Rows;i++)
            for (int j=0;j<Colomns;j++){
                if ((arrayOfGame[i*Colomns+j]==1)||(arrayOfGame[i*Colomns+j]==3))//(arrayOfGame[i*Colomns+j]==18)||(arrayOfGame[i*Colomns+j]==19)||(arrayOfGame[i*Colomns+j]==17))
                    playero=playero+1;
                if ((arrayOfGame[i*Colomns+j]==2)||(arrayOfGame[i*Colomns+j]==4))//||(arrayOfGame[i*Colomns+j]==28)||(arrayOfGame[i*Colomns+j]==29)||(arrayOfGame[i*Colomns+j]==27))
                    playert++;
                if ((arrayOfGame[i*Colomns+j]>15)&&(arrayOfGame[i*Colomns+j]<20))
                    playero++;
                if ((arrayOfGame[i*Colomns+j]>25)&&(arrayOfGame[i*Colomns+j]<30))
                    playert++;
            }
        if (player==1) {
            text.setText("\n     "+Integer.toString(playero) + "/" + Integer.toString(playert));
            text.setTextColor(Color.RED);
        }
        if (player==2) {
            text.setText("\n     "+Integer.toString(playert) + "/" + Integer.toString(playero));
            text.setTextColor(Color.GREEN);
        }
        if (playero<=playert)
            redwinner=true;
        else
            redwinner=false;
    }
    public boolean scanvoid(){
        int a=0;
        for (int i=0;i<Rows;i++) {
            for (int j = 0; j < Colomns; j++) {
                if (arrayOfGame[i * Colomns + j] == 0)
                    a++;
                if (a > 2)
                    return true;
            }
        }
        return false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        Button button = (Button)findViewById(R.id.button2);
        button.setText("Exit");
        playerrun = (ImageButton) findViewById(R.id.playerrun);
        text = (TextView) findViewById(R.id.textView);
        text.setVisibility(View.GONE);
        topforcity=false;
        File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard,"castles.txt");
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }
        String a = text.toString();
        String[] words = a.split("\\s");
        RelativeLayout flay = new RelativeLayout(getApplicationContext());
        try{
            Colomns = Integer.parseInt(words[0]);
            Rows = Integer.parseInt(words[1]);
            Turns = Integer.parseInt(words[2]);
            int mode = Integer.parseInt(words[3]);
            arrayOfImage = new ImageButton[Colomns * Rows];
            arrayOfGame = new int[Colomns * Rows];

            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int scalex = metrics.widthPixels;
            int scaley = metrics.heightPixels;
            float dp = metrics.density;
            int scale;
            if (scalex < scaley) scale = scaley;
            else scale = scalex;
            HorizontalScrollView horizontalScrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
            ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
            int height = (int) (scale * Rows * 0.089 + 0.0435 * scale);
            int width = (int) (0.078 * Colomns * scale + 0.025 * scale);
            scrollView.addView(flay, width, height);
//        Log.d("ImageView3:", "Width"+(((ImageView)findViewById(R.id.ImageView3)).getWidth()));
            if (width < scalex - 119 * dp - 106 * dp) {
                scrollView.setX((int) (((scalex - width - 119 * dp - 106 * dp) / 2)));
            }
            if (height < scaley) {
                scrollView.setY((int) ((scaley - height) / 2));
            }
            for (int i = 0; i < Rows; i++) {
                int b = 0;
                for (int j = 0; j < Colomns; j++) {
                    arrayOfImage[i * Colomns + j] = new ImageButton(getApplicationContext());
                    flay.addView(arrayOfImage[i * Colomns + j], 100, 87);
                    arrayOfImage[i * Colomns + j].setBackground(null);
                    arrayOfImage[i * Colomns + j].setImageResource(R.drawable.card);
                    arrayOfImage[i * Colomns + j].setClickable(true);
                    arrayOfImage[i * Colomns + j].setX(74f * j);
                    arrayOfImage[i * Colomns + j].setId(i * Colomns + j);
                    arrayOfImage[i * Colomns + j].setScaleType(ImageView.ScaleType.FIT_CENTER);
                    if (b == 0) {
                        arrayOfImage[i * Colomns + j].setY(89f * i);
                        b = 1;
                    } else {
                        arrayOfImage[i * Colomns + j].setY(89f * i + 43f);
                        b = 0;
                    }
                }
            }
            Log.e("SSS", words[3]);
            arrayOfGame[(Integer.parseInt(words[4]) - 1) * Colomns + (Integer.parseInt(words[5]) - 1)] = 1;
            arrayOfImage[(Integer.parseInt(words[4]) - 1) * Colomns + (Integer.parseInt(words[5])) - 1].setImageResource(R.drawable.city_pl1);
            addmycontrol(Integer.parseInt(words[4]) - 1, Integer.parseInt(words[5]) - 1);
            player = 2;
            arrayOfGame[(Integer.parseInt(words[6]) - 1) * Colomns + Integer.parseInt(words[7]) - 1] = 2;
            arrayOfImage[(Integer.parseInt(words[6]) - 1) * Colomns + Integer.parseInt(words[7]) - 1].setImageResource(R.drawable.city_pl2);
            addmycontrol(Integer.parseInt(words[6]) - 1, Integer.parseInt(words[7]) - 1);
            player = 1;
            int x = 8;
            if (mode == 2) {
                int mountains = Integer.parseInt(words[8]);
                int rivers = Integer.parseInt(words[9]);
                x = 10;
                for (int i = 0; i < mountains; i++) {
                    arrayOfGame[(Integer.parseInt(words[i * 2 + x]) - 1) * Colomns + Integer.parseInt(words[i * 2 + x + 1]) - 1] = 5;
                    arrayOfImage[(Integer.parseInt(words[i * 2 + x]) - 1) * Colomns + Integer.parseInt(words[i * 2 + x + 1]) - 1].setImageResource(R.drawable.mountain_nopl);
                }
                x = x + mountains * 2;
                for (int i = 0; i < rivers; i++) {
                    arrayOfGame[(Integer.parseInt(words[i * 2 + x]) - 1) * Colomns + Integer.parseInt(words[i * 2 + x + 1]) - 1] = 9;
                    arrayOfImage[(Integer.parseInt(words[i * 2 + x]) - 1) * Colomns + Integer.parseInt(words[i * 2 + x + 1]) - 1].setImageResource(R.drawable.river_nopl);
                }
                x = x + rivers * 2;
            }
            Turns = Turns * 2;
            for (int i = 0; i < Turns; i++) {
                addmycastle(Integer.parseInt(words[i * 2 + x + 1]) - 1, Integer.parseInt(words[i * 2 + x]) - 1);
            }
        }catch (IndexOutOfBoundsException e){
            Intent intent = new Intent(ReadFromFile.this,MainActivity.class);
            Toast.makeText(ReadFromFile.this,"Can't do smth",Toast.LENGTH_SHORT).show();
            startActivity(intent);
        }catch (Exception e){
            Intent intent = new Intent(ReadFromFile.this,MainActivity.class);
            Toast.makeText(ReadFromFile.this,"Can't do smth",Toast.LENGTH_SHORT).show();
            startActivity(intent);
        }
        scan();
        scanvoid();
        flay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReadFromFile.this, MainActivity.class);
                startActivity(intent);
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(ReadFromFile.this);
        builder.setTitle(" ");
        if (redwinner) {
            builder.setMessage("RED WIN");
            builder.setIcon(R.drawable.city_pl2);
        }
        else {
            builder.setMessage("GREEN WIN");
            builder.setIcon(R.drawable.city_pl1);
        }
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                Intent intent = new Intent(ReadFromFile.this, MainActivity.class);
                startActivity(intent);
                dialog.cancel();
            }
        });
        builder.setNegativeButton("Show", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });
        builder.setCancelable(false);
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void endOfPartClick(View view) {
        Intent intent = new Intent(ReadFromFile.this, MainActivity.class);
        startActivity(intent);
    }
}