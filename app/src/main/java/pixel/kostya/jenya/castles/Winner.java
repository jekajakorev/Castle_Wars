package pixel.kostya.jenya.castles;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by shpp-admin on 28.10.2016.
 */

public class Winner extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.winner);
        if (getIntent().getExtras().getInt("Winner")==1){
            ((RelativeLayout)findViewById(R.id.winner)).setBackgroundResource(R.drawable.win_green);
            ((TextViewPlus)findViewById(R.id.textViewPlus)).setText("Green");
        }
        else {
            ((RelativeLayout) findViewById(R.id.winner)).setBackgroundResource(R.drawable.win_red);
            ((TextViewPlus) findViewById(R.id.textViewPlus)).setText("Red");
        }
    }
    public void onMainActivityTop(View view){
        Intent intent = new Intent(Winner.this,MainActivity.class);
        startActivity(intent);
    }
    public void onRestartClick(View view){
        switch (getIntent().getExtras().getInt("Mode")){
            case 1:Intent intent = new Intent(Winner.this,ActivityGame.class);
                intent.putExtra("arraygame", getIntent().getExtras().getIntArray("arraygame"));
                intent.putExtra("Turns",getIntent().getExtras().getInt("Turns"));
                intent.putExtra("Colomns",getIntent().getExtras().getInt("Colomns"));
                intent.putExtra("Rows",getIntent().getExtras().getInt("Rows"));
                startActivity(intent);break;
            case 2:intent = new Intent(Winner.this,ActivityGameBot.class);
                intent.putExtra("arraygame", getIntent().getExtras().getIntArray("arraygame"));
                intent.putExtra("Turns",getIntent().getExtras().getInt("Turns"));
                intent.putExtra("Colomns",getIntent().getExtras().getInt("Colomns"));
                intent.putExtra("Rows",getIntent().getExtras().getInt("Rows"));
                startActivity(intent);break;
            case 3: intent = new Intent(Winner.this,ActivityGameGenerals.class);
                intent.putExtra("arraygame", getIntent().getExtras().getIntArray("arraygame"));
                intent.putExtra("Turns",getIntent().getExtras().getInt("Turns"));
                intent.putExtra("Colomns",getIntent().getExtras().getInt("Colomns"));
                intent.putExtra("Rows",getIntent().getExtras().getInt("Rows"));
                startActivity(intent);break;
            case 4: intent = new Intent(Winner.this,ActivityGameBotRedFirst.class);
                intent.putExtra("arraygame", getIntent().getExtras().getIntArray("arraygame"));
                intent.putExtra("Turns",getIntent().getExtras().getInt("Turns"));
                intent.putExtra("Colomns",getIntent().getExtras().getInt("Colomns"));
                intent.putExtra("Rows",getIntent().getExtras().getInt("Rows"));
                startActivity(intent);break;
        }

    }
}
